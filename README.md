### Spinning up a Chatops test environment

This rough documentation can be used for testing [Chatops](https://gitlab.com/gitlab-com/chatops.git) with your own GitLab instance.

##### A high level overview of how Chatops works

When a new commit is made to the Chatops project, a new pipeline run is triggered which creates a new docker image that ends up within the container registry for the project. When a Chatops command is run from Slack, the docker image in the container registry is invoked, and this calls an admin access token stored within a CI/CD variable, which is used to speak to the GitLab instance API.

##### Setup

1. Import the [Chatops repository](https://gitlab.com/gitlab-com/chatops.git) into your self-managed GitLab instance.

1. The project uses `chatops` under the `tags` within it's [.gitlab-ci.yml](https://gitlab.com/gitlab-com/chatops/-/blob/master/.gitlab-ci.yml), so make sure you have an available runner with the `chatops` tag.

1. Chatops uses "docker-in-docker", so you may see docker errors on pipeline runs initially. For example, you'll see the following error in your `build:image` job:

    ```
    error during connect: Post "http://docker:2375/v1.24/blah_etc_etc": dial tcp: lookup docker on 169.254.169.254:53: no such host
    ```

    Refer to the [Use Docker Socket Binding documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding) on how to fix this. All we should need to do for now is make sure the GitLab runner with the     `chatops` tag, has the following set inside it's `/etc/gitlab-runner/config.toml` file, under the `[runners.docker]` section:

    ```
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    ```

1. In the Chatops repository, set your GitLab instance's hostname inside `lib/chatops/gitlab/client.rb` for `DEFAULT_HOST`

1. [Create a Slack workspace](https://slack.com/get-started#/createnew) for testing

1. Follow the documentation for setting up [Slack Slash Commands](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html). When you're at the step that advises you to create a Slash Command Integration, use `/chatops` as your trigger command.


1. Create a Slack Bot with the [Slack Bots App](https://gitlab.slack.com/apps/A0F7YS25R-bots), and add it to your Slack Workspace.

1. Grab the API Token that is generated for the bot.

1. Make sure the bot is invited into the channel you intend to use `/chatops` commands in.

1. Inside the Chatops project go to `Settings > CI/CD > Variables > Add variable` add the following variables:

    ```
    Key   = SLACK_TOKEN
    Value = <API-token-generated-by-the-slack-bot>
    ```

    ```
    Key   = CHAT_CHANNEL
    Value = The content of the last part of the URL for the Slack channel you've invited the bot into - eg https://app.slack.com/client/SOMETHINGSOMETHING/<CHAT_CHANNEL>
    ```

    ```
    Key   = GITLAB_TOKEN
    Value = Create a personal access token with the API scope, linked to a GitLab user account with admin rights. Enter the generated token here.
    ```

1. Try running `/chatops run help` inside the Slack channel you've created, that the Slack bot was also invited to. You should get a response from the Slash Command Integration stating `Before I do anything for you, please connect your GitLab account.` - Follow the URL, and click `Authorize` on the page you're led to on your GitLab instance.

1. Try running a Chatops user command such as `/chatops run user find exampleuser`, if everything is working as expected, you'll recieve a valid response from the Slack bot inside the Slack channel.
